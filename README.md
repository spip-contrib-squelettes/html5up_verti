# HTML5UP Verti pour SPIP

Adaptation pour SPIP du squelette « Verti » de html5up [html5up.net/verti](https://html5up.net/verti)

## Documentation
https://contrib.spip.net/xxxx

## TO-DO:
 - Création de modèles de boutons
 - Créer une page de recherche
 - Créer une page de contact
 - Gérer les sous-rubriques et les afficher dans le quelette features
