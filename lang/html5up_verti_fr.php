<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
'html5up_verti_titre' => 'Configurer Verti',

//F
'footer_liens_recents' => 'Dernières modifications',
'footer_titre_contact' => 'Contacter nous',

//S
'sidebar_plus_hasard' => 'Au hasard dans la catégorie ...',
'sidebar_plus_infos' => 'Voir tout',
'sommaire_savoir_plus' => 'En savoir plus',
);
