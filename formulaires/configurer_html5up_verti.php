<?php 

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies.
 * @return array
 **/
function formulaires_configurer_html5up_verti_saisies_dist(): array {
    // $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
		$saisies = [
			[// Accueil
			'saisie' => 'fieldset',
				'options' => [
					'nom' => 'sommaire',
					'label' => '<:public:accueil_site:>'
				],
				'saisies' => [
					// Articles du projecteur pas 
					['saisie' => 'selecteur_article',
						'options' => [
							'nom' => 'sommaire_articles_projecteur',
							'label' => '<:html5up_erti:sommaire_articles_projecteur:>',
							'explication' => '<:html5up_verti:sommaire_articles_projecteur_explication:>',
							'multiple' => false
						]
					]
				],
				'saisies' => [
					// Article en fin de main page avec sidebar
					['saisie' => 'selecteur_article',
						'options' => [
							'nom' => 'sommaire_article_principal',
							'label' => '<:html5up_erti:sommaire_article_principal:>',
							'explication' => '<:html5up_verti:sommaire_article_principal_explication:>',
							'multiple' => false
						]
					]
				]
			]
		];
	return $saisies;
}